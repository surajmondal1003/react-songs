import { combineReducers } from 'redux';
//Reducers

const songsReducer = () => {

    return [
        {
            title:'No scrubs' , 
            duration : '4:05'
        },
        {
            title:'I want it that way' , 
            duration : '3:05'
        },
        {
            title:'All star' , 
            duration : '2:30'
        }
       
    ];
};


const selectedSongreducer = (selectedSong=null,action)=>{
    if (action.type === 'SONG_SELECTED'){
        return action.payload;
    } 
    return selectedSong;
}


export default combineReducers({
    songs: songsReducer,
    selectedSong:selectedSongreducer
});